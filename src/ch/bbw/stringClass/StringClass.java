package ch.bbw.stringClass;

public class StringClass {
	public StringClass() {
		String wanze = "Wanze";
		String tanzen = "tanzen";
		String song = "Auf der Mauer, auf der Lauer, sitzt 'ne kleine " + wanze + "\n" +
				"Sieh einmal die " + wanze + " an, wie die Wanze " + tanzen + " kann\n" +
				"Auf der Mauer, auf der Lauer, sitzt 'ne kleine " + wanze + "\n";
		//String newSong;
		System.out.println(song);
		song = song.replace(wanze, wanze.substring(0, wanze.length() - 1));
		song = song.replace(tanzen, tanzen.substring(0, tanzen.length() - 2));
		wanze = wanze.substring(0, wanze.length() - 1);
		tanzen = tanzen.substring(0, tanzen.length() - 2);
		System.out.println(song);
		while (wanze.length() > 1){
			song = song.replace(wanze, wanze.substring(0, wanze.length() - 1));
			song = song.replace(tanzen, tanzen.substring(0, tanzen.length() - 1));
			wanze = wanze.substring(0, wanze.length() - 1);
			tanzen = tanzen.substring(0, tanzen.length() - 1);
			System.out.println(song);
		}
		song = song.replace("W", "()");
		song = song.replace(" t", "()");
		System.out.println(song);
	}
}
